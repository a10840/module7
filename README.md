# Подготовка #
1. Скопировать файл .env.example в .env и заполнить переменные
2. Скопировать файл rsyslog_config/graylog.conf в /etc/rsyslog.d/
3. Перезапустить сервис rsyslog командой sudo systemctl restart rsyslog
4. Запустить систему сбора логов командой docler-compose up -d
5. Подождать запуска сервиса и перейти в браузере по адресу 127.0.0.1
6. Залогиниться в Graylog, логин - admin, пароль - тот, который сгенерировали 
   ранее при заполнении .env файла.
7. В web-интерфейсе graylog перейти в System -> Content Packs, нажать Upload 
   выбрать файл content_pack/Skillbox_inputs+events.json и загрузить еге, 
   нажав на кнопку Upload
8. Нажать Install напротив загруженного ранее пакета и нажать Install еще раз
9. Перейти в System -> Inputs и запустить неактивные инпуты. 

# Описание #

## Инпуты ##

### nginx-syslog ###
Получает access-log и error-log из контейнера с Nginx

### Docker ###
Получает логи с докер-контейнеров данного окружения

### Syslog ###
Получает логи с хостовой машины через ранее настроенный rsyslog

## Эвенты ##

### POST request detected ###
Срабатывает при появлении в access-log Nginx POST запроса

### SSH session started ###
Срабатывает при подклчюении к хостовой машине по SSH любого пользователя

